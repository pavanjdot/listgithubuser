import React from 'react'
import axios from 'axios'
import List from './List'


class Search extends React.Component{
    state = {user: '', data:[], total: null, per_page: null, current_page: null}

    componentDidMount(){
        this.makeHttpRequestWithPage(1);
    }

    handleChange = data => {
        this.setState({
            user: data
        })
    }

    makeHttpRequestWithPage = async(pagenumber) => {
        
        const username = this.state.user
        axios.get(`https://api.github.com/search/users?q=${username}&page=${pagenumber}`).then(resp => {
            
           this.setState({
               data: resp.data.items,
               total: resp.total,
               per_page: resp.per_page,
               current_page: resp.page,
           });
        
        
        })
         
       }

    render(){
    
        
        
        return(
            <div className="custom">
                <div className="ui input focus"> 
                <input type="text" placeholder="search" onChange={e => {this.handleChange(e.target.value)}}/>
                
                </div> 
            
            <List data={this.state.data}/>

            <div className="pagination">
                <span onClick={() => this.makeHttpRequestWithPage(1)}>1</span>
                 <span onClick={() => this.makeHttpRequestWithPage(2)}>2</span>
                 <span onClick={() => this.makeHttpRequestWithPage(3)}>3</span>
                 <span onClick={() => this.makeHttpRequestWithPage(4)}>4</span>
                 <span onClick={() => this.makeHttpRequestWithPage(5)}>5</span>
                 <span onClick={() => this.makeHttpRequestWithPage(6)}>6</span>
                 <span onClick={() => this.makeHttpRequestWithPage(7)}>7</span>
                 <span onClick={() => this.makeHttpRequestWithPage(8)}>8</span>
                 <span onClick={() => this.makeHttpRequestWithPage(4)}>9</span>
                 <span onClick={() => this.makeHttpRequestWithPage(4)}>10</span>

                 </div>
           

            </div>
           
            
        );
    }
}

export default Search;