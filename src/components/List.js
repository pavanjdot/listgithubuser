import React from 'react'
import '../App.css'

class List extends React.Component{
    
    render(){
        return(
            this.props.data.map(list => {
                return(
                    <div className="ui relaxed divided list">
                    <div className="item">
                    <img class="ui avatar image" src={list.avatar_url}></img>
                    <div className="content">
                    <a className="header">{list.login}</a>
                    <div className="description">ID:{list.id}</div>
                    </div>
                    </div>
                    </div>  
                    );
                    })


        );  
    }
}       

export default List